#ifndef PIECHART_H_
#define PIECHART_H_

#include "task.h"

class TaskSummaryPieChart : public QGraphicsItem
{
public:
    TaskSummaryPieChart(QGraphicsItem* parent = NULL) : QGraphicsItem(parent)
    {
        setAcceptsHoverEvents(true);
    }
    QRectF boundingRect() const { return QRectF(-0.61,-0.6,1.2,1.2);}
    void setHits(const QList<HitElement>& hits)
    {
        qDeleteAll(m_subItems);
        m_subItems.clear();

        // compute total values per task
        double total=0;
        QMap<Task*, double> totals;
        QMap<Task*, QMap<QString, double> > partials;
        for(QList<HitElement>::const_iterator itr = hits.begin(); itr!= hits.end(); itr++) {
            const HitElement& element = (*itr);
            if (element.task->title == "<away>")
                continue;
            total += element.duration;
            totals[element.task] = totals.value(element.task, 0) + element.duration;
            partials[element.task][element.subtask] += element.duration/60.0;
        }
        // sort total values
        QMap<double, Task*> sortedTotals;
        for(QMap<Task*, double>::const_iterator titr = totals.begin(); titr!=totals.end(); titr++) {
            sortedTotals.insertMulti(titr.value(), titr.key());
        }
        double startAngle = 90;
        for(QMap<double, Task*>::const_iterator itr = sortedTotals.begin(); itr != sortedTotals.end(); itr++) {
            Task* task = (*itr);
            double spanAngle = itr.key() / (total) * 360;
            QGraphicsPathItem* pathItem = new QGraphicsPathItem(this);
            scene()->addItem(pathItem);
            QString subtasksTip;
            const QMap<QString, double>& taskPartials(partials[task]);
            for(QMap<QString, double>::const_iterator itr = taskPartials.begin(); itr!=taskPartials.end(); itr++) {
                subtasksTip += QString("<li>%1 : %2</li>").arg(itr.value(),5,'g',2).arg(itr.key());
            }
            pathItem->setToolTip("<b>" + task->title + "</b> : " + QString(" %1 / %2 hours</p><ul>%3").arg(itr.key()/60.0).arg(total/60.0).arg(subtasksTip));
            QPainterPath path;
            QPainterPath p;
            p.moveTo(0,0);
            p.arcTo(boundingRect(), startAngle, spanAngle);
            p.closeSubpath();
            pathItem->setPath(p);
            pathItem->setBrush(task->bgColor);
            pathItem->setPen(QPen(QBrush(task->fgColor),0));
            startAngle += spanAngle;
            m_subItems.insert(task , pathItem);
        }
    }
    virtual void paint ( QPainter * painter, const QStyleOptionGraphicsItem * /* option */, QWidget * /* widget */ )
    {
    }
//    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);

protected:
    QMap<Task*, QGraphicsPathItem*> m_subItems;
};

#endif
