#ifndef PIXMAPVIEVWER_H_
#define PIXMAPVIEVWER_H_

#include <QtGui>

class PixmapViewer : public QWidget
{
    Q_OBJECT;
    QPixmap m_p;
    bool m_scaling;
public:
    PixmapViewer(QWidget* parent = 0, bool scaling=true) : QWidget(parent) {
        setMinimumWidth(150);
        setMinimumHeight(150);
        m_scaling = scaling;
    }
    void setPixmap(const QPixmap& p) {
        if (m_scaling) {
            m_p = p.scaled(qMin(p.width(), 400),qMin(p.height(),400),Qt::KeepAspectRatio);
        } else {
            m_p = p;
        }
        update();
    }
    QPixmap pixmap() const { return m_p; }
    void paintEvent(QPaintEvent* );
signals:
    void changed();
protected:
//    void keyPressEvent(QKeyEvent*);
    void mousePressEvent(QMouseEvent* e);
};




#endif
